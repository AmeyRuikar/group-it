package com.apps.ameyruikar.myfirstapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Locale;
import java.util.*;


import static android.app.PendingIntent.getActivity;


public class MyActivity extends AppCompatActivity {

    private EditText address_input, test_address;
    private  String address;
    private Context mContext;
    final ArrayList selectedPlaces = new ArrayList<String>();
    int numberOfClusters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my);

        final AutoCompleteTextView autocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete);
        autocompleteView.setAdapter(new PlaceAutoCompleteAdapter(MyActivity.this, R.layout.autocomplete_list));

        mContext = this;



        //Array to hold selected places


        //Adapter for selectedPlaces
        final ArrayAdapter adapter = new ArrayAdapter(mContext, android.R.layout.simple_list_item_1, selectedPlaces);


        //List
        ListView Lv = (ListView) findViewById(R.id.List);
        Lv.setAdapter(adapter);

        autocompleteView.setThreshold(4);

        try {

            autocompleteView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    String desc = (String) parent.getItemAtPosition(position);

                    //add to my listVIew
                    selectedPlaces.add(desc);
                    adapter.notifyDataSetChanged();
                   // Toast.makeText(mContext, "Destination added->" + desc, Toast.LENGTH_SHORT).show();
                    autocompleteView.clearListSelection();
                    autocompleteView.setText("");

                }
            });
        }
        catch (Exception e){

          //  Log.i("MyActivity", "AdapterError" + e);

        }

        Lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                //Toast.makeText( mContext, "position"+  selectedPlaces.get(position) ,Toast.LENGTH_SHORT).show();

                Log.i("dialog", "clicked");

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Remove Destination");
                builder.setMessage("Are you sure?");

                builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        selectedPlaces.remove(position);
                        adapter.notifyDataSetChanged();

                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });


                AlertDialog alert = builder.create();

                alert.show();


            }
        });



/*
        address_input.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                address  = s.toString();
            }
        });
*/


    }

    @Override
    public void onBackPressed()
    {

        // super.onBackPressed(); // Comment this super call to avoid calling finish()
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void getLongLat(View view) {



        //Toast.makeText(mContext, "fortune 500", Toast.LENGTH_SHORT).show();
        //Now iterate through the entire List view and get each destination's lat long.

        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        final EditText ed = new EditText(mContext);

        ed.setInputType(InputType.TYPE_CLASS_NUMBER);

        builder.setTitle("Groups");
        builder.setMessage("Enter the number of groupings you want");

        builder.setView(ed);

        builder.setPositiveButton("Go", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String temp = String.valueOf(ed.getText());




                numberOfClusters = Integer.parseInt(temp);

                dialog.dismiss();

                if (numberOfClusters > 0 && numberOfClusters < selectedPlaces.size()) {

                    double lat = 0.0;
                    double lng = 0.0;
                    kmeans clustering = new kmeans(numberOfClusters);
                    List<point> clusteredPoints = new ArrayList<point>();

                    Geocoder geoCoder = new Geocoder(mContext, Locale.getDefault());

                    try {

                        for (int i = 0; i < selectedPlaces.size(); i++) {

                            List<Address> addresses = geoCoder.getFromLocationName(selectedPlaces.get(i).toString(), 1);
                            Log.i("MyActivity", addresses.toString());
                            //Toast.makeText(mContext, "Latitude:" + addresses.get(0).getLatitude(), Toast.LENGTH_SHORT).show();


                            clustering.addPoint(new point(selectedPlaces.get(i).toString(), addresses.get(0).getLatitude(), addresses.get(0).getLongitude(), -1));

                        }


                        clustering.cluster();
                        clusteredPoints = clustering.getListOfPoints();
                        for (point p : clusteredPoints) {
                            Log.i("Listofpoints", String.valueOf(p.getColor()));

                        }
                    } catch (Exception e) {
                        Log.i("MyActivity", "Geocode" + e);
                    } finally {

                        Intent i = new Intent(mContext, MapsActivity.class);

                        i.putExtra("size", clusteredPoints.size());

                        for (int j = 0; j < clusteredPoints.size(); j++) {


                            i.putExtra(String.valueOf(j), clusteredPoints.get(j));

                        }
                        startActivity(i);


                    }
                } else {

                    Toast.makeText(mContext,"Wrong values for groups",Toast.LENGTH_SHORT).show();

                }

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // String temp = String.valueOf(e.getText());

//                numberOfClusters = Integer.parseInt(temp);
                dialog.dismiss();

            }
        });

        builder.show();







    }
}