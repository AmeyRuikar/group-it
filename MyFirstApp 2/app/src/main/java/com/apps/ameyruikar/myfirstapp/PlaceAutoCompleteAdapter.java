package com.apps.ameyruikar.myfirstapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.logging.Filter;
import java.util.logging.LogRecord;

/**
 * Created by ameyruikar on 8/31/15.
 */
public class PlaceAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    ArrayList<String> resultList;
    Context current;
    int res;

    //get object of places api

    placeAPI place = new placeAPI();

    public PlaceAutoCompleteAdapter(Context context, int resource) {
        super(context, resource);

        current = context;
        res = resource;
    }

    @Override
    public String getItem(int position){

        return resultList.get(position);
    }

    public int getCount(){

        return resultList.size();
    }


    //decomposingw
    public android.widget.Filter getFilter(){

        android.widget.Filter filter = new android.widget.Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults=new FilterResults();

                    if(constraint != null){

                            resultList = place.autocomplete(constraint.toString());

                            Log.i("response",resultList.toString());
                            // Footer google policies
                            resultList.add("powered by Google");

                            filterResults.values = resultList;
                            filterResults.count = resultList.size();

                    }


                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                    if( results!= null && results.count > 0)
                    {
                        notifyDataSetChanged();
                    }
                    else{

                        notifyDataSetInvalidated();
                    }


            }
        };

        return filter;

    }

}
