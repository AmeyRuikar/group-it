package com.apps.ameyruikar.myfirstapp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class kmeans {
	public List<point> listOfPoints=new ArrayList<point>();	
	public List<point> listOfMeans=new ArrayList<point>();
	public int k;
	
	public kmeans(int k) {
		this.k=k;
	}
	
	public void addPoint(point P){
		listOfPoints.add(P);
	}
	
	public List<point> getListOfPoints() {
		return listOfPoints;
	}

	public void setListOfPoints(List<point> listOfPoints) {
		this.listOfPoints = listOfPoints;
	}

	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.k = k;
	}
	
	private static void getSubsets(List<point> superSet, int k, int idx, Set<point> current,List<Set<point>> solution) {
	    //successful stop clause
	    if (current.size() == k) {
	        solution.add(new HashSet<>(current));
	        return;
	    }
	    //unseccessful stop clause
	    if (idx == superSet.size()) return;
	    point x = superSet.get(idx);
	    current.add(x);
	    //"guess" x is in the subset
	    getSubsets(superSet, k, idx+1, current, solution);
	    current.remove(x);
	    //"guess" x is not in the subset
	    getSubsets(superSet, k, idx+1, current, solution);
	}

	public static List<Set<point>> getSubsets(List<point> superSet, int k) {
	    List<Set<point>> res = new ArrayList<>();
	    getSubsets(superSet, k, 0, new HashSet<point>(), res);
	    return res;
	}

	private void init(){
		Random random=new Random();
		int sizeOfPoints=listOfPoints.size();
		while(listOfMeans.size() != k){
			int tempIndex=random.nextInt(sizeOfPoints);
			if(!listOfMeans.contains(listOfPoints.get(tempIndex))){
				listOfMeans.add(listOfPoints.get(tempIndex));
			}
		}
	}
	
	public point findCentre(List<point> L){
		double x=0,y=0,z=0;
		int sizeOfList = L.size();
		for(int i=0;i<sizeOfList;i++){
			double lat=Math.toRadians(L.get(i).getLatitude());
			double lon=Math.toRadians(L.get(i).getLongitude());
			x=x+(Math.cos(lat)*Math.cos(lon));
			y=y+(Math.cos(lat)*Math.sin(lon));
			z=z+(Math.sin(lat));
		}
		x=x/sizeOfList;
		y=y/sizeOfList;
		z=z/sizeOfList;
		double lon=Math.atan2(y, x);
		double hyp=Math.sqrt(x*x + y*y);
		double lat=Math.atan2(z,hyp);
		
		return new point(Math.toDegrees(lat),Math.toDegrees(lon));
	}
	
	private double calculateError(){
		double ans=0;
		
		for(point p:this.listOfPoints){
			int color=p.getColor();
			point centroid = this.listOfMeans.get(color);
			ans = ans + p.distance(centroid);
		}
		
		return ans;
	}
	
	private void copy(List<point> source , List<point> destination){
		destination.clear();
		for(point p: source){
			destination.add(p);
		}
	}
	
	public void cluster(){
		
		int numberOfPoints=listOfPoints.size();
		double globalError=1000000;
		List<point> idealList = new ArrayList<point>();
		List<Set<point>> LP = new ArrayList<>();
		List<point> temp = new ArrayList<point>();
		LP=kmeans.getSubsets(this.listOfPoints, this.k);
		int ll=LP.size();
		for(int l=0;l<ll;l++){
			//this.init();
			temp.addAll(LP.get(l));
			this.copy(temp, this.listOfMeans);
			int stop=0;
			while(stop==0){
				stop=1;
				
				//Assign a color to each point depending upon its distance from the means
				for(int i=0;i<numberOfPoints;i++){
					double mindist=1000000,tempdist;
					int whichcolor=listOfPoints.get(i).getColor();
					
					for(int j=0;j<k;j++){
						tempdist=listOfPoints.get(i).distance(listOfMeans.get(j));
						if(tempdist < mindist){
							mindist=tempdist;
							whichcolor=j;
						}
					}
					
					if(whichcolor != listOfPoints.get(i).getColor()){
						stop=0;
						listOfPoints.get(i).setColor(whichcolor);
					}
				}
				
				//Update step. Make a list of all points for one color and use findCentre method
				for(int j=0;j<k;j++){
					List<point> tempList=new ArrayList<point>();
					
					for(int i=0;i<numberOfPoints;i++){
						if(listOfPoints.get(i).getColor() == j){
							tempList.add(listOfPoints.get(i));
						}
					}
					point mid = this.findCentre(tempList);
					listOfMeans.set(j, mid);
					
				}
				
			}
			
			double tempError = this.calculateError();
			if(tempError < globalError){
				globalError=tempError;
				this.copy(this.listOfPoints, idealList);
			}
		}
		
		this.copy(idealList, this.listOfPoints);
	}
	
	public void printMeans(){
		for(int i=0;i<listOfMeans.size();i++){
			System.out.println(listOfMeans.get(i).getLatitude());
		}
	}
	
	public void printPoints(){
		for(int i=0;i<listOfPoints.size();i++){
			System.out.println(listOfPoints.get(i).getName()+" "+listOfPoints.get(i).getLatitude()+","+listOfPoints.get(i).getLongitude()+" Color = "+listOfPoints.get(i).getColor());;
		}
	}
}
