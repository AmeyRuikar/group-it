package com.apps.ameyruikar.myfirstapp;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.*;

import android.widget.*;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.*;

public class MapsActivity extends FragmentActivity {


    double[] color = {0.0, 240.0, 120.0, 60.0, 188.0, 270.0};

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    List<point> ListOfPoints = new ArrayList<point>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        Bundle bundle = getIntent().getExtras();

        int sizeOfArray = bundle.getInt("size");


        for (int j = 0; j < sizeOfArray; j++) {

            point temp = new point();

            temp = bundle.getParcelable(String.valueOf(j));

            ListOfPoints.add(temp);

            Log.i("maps", temp.toString());

        }


        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {

        try {

            for (int j = 0; j < ListOfPoints.size(); j++) {

                mMap.addMarker(new MarkerOptions().position(new LatLng(ListOfPoints.get(j).getLatitude(), ListOfPoints.get(j).getLongitude())).icon(BitmapDescriptorFactory.defaultMarker((float) color[ListOfPoints.get(j).getColor()])));

            }
            point temp = new point();
            kmeans k_temps = new kmeans(1);

            temp = k_temps.findCentre(ListOfPoints);


            CameraUpdate centre = CameraUpdateFactory.newLatLng(new LatLng(temp.getLatitude(), temp.getLongitude()));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
            mMap.moveCamera(centre);
            mMap.animateCamera(zoom);

            Toast.makeText(this, "Locations marked with the same color have been clubbed together", Toast.LENGTH_LONG).show();
            // mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        } catch (Exception e) {
            Log.i("maps", ""+e);
        }
    }
}
