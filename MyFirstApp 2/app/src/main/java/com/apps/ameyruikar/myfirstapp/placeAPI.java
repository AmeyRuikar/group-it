package com.apps.ameyruikar.myfirstapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by ameyruikar on 8/24/15.
 */
public class placeAPI {


    private String webservice = "https://maps.googleapis.com/maps/api/place";
    private String API_key = "AIzaSyCrvF3hUh6eycVrbjQoGVBQqQgI76YoISg";
    private String type = "/autocomplete";
    private String json = "/json";



    public ArrayList<String> autocomplete(String input){

        //result from the GeodataAPI calls
        ArrayList<String> predictions = null;

        StringBuilder jsonresults = new StringBuilder();

        HttpURLConnection conn = null;

        try {

            StringBuilder str = new StringBuilder(webservice + type + json);

            //build the url with input/s
            str.append("?key=" + API_key);
            //str.append("&types=()");
            str.append("&input=" + URLEncoder.encode(input, "UTF-8"));

            //string converted to url
            URL url =new URL(str.toString());

            //open connection
            conn = (HttpURLConnection) url.openConnection();

            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            int read;
            char[] buf = new char[1024];

            while( (read=in.read(buf))!= -1){

                jsonresults.append(buf, 0 ,read);

                Log.i("response", jsonresults.toString());
            }



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {

            conn.disconnect();
        }


        try{

            JSONObject jObj = new JSONObject(jsonresults.toString());

            //the returned JSON has predictions key followed by description
            JSONArray jArray = jObj.getJSONArray("predictions");

            predictions = new ArrayList<String>(jArray.length());


            for(int i = 0; i < jArray.length(); i++){

                predictions.add(jArray.getJSONObject(i).getString("description"));
               // placeID.add(jArray.getJSONObject(i).getString("place_id"));
                Log.i("MyActivity", jArray.getJSONObject(i).getString("description"));
               // Log.i("MyActivity", jArray.getJSONObject(i).getString("place_id"));
            }


        }catch (Exception e){

            Log.i("MyActivity","PLACE API" +  e);
            e.printStackTrace();

        }


        return predictions;
    }



}
