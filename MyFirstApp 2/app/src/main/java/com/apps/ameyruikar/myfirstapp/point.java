package com.apps.ameyruikar.myfirstapp;

import android.os.Parcel;
import android.os.Parcelable;

public class point implements Parcelable{
	private double latitude, longitude;
	private int color;
	public String name;

	public point(String name, double latitude, double longitude, int color) {
		this.name=name;
		this.latitude=latitude;
		this.longitude=longitude;
		this.color=color;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public point(double latitude, double longitude){
		this.name="Undefined";
		this.latitude=latitude;
		this.longitude=longitude;
		this.color=-1;
	}
	
	public point(String name, double latitude, double longitude) {
		this.name=name;
		this.latitude=latitude;
		this.longitude=longitude;
		this.color=-1;
	}

	public point(){

	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
	
	public double distance(point P) {
		double R = 6372.8;		
		double lat1=this.getLatitude(),lat2=P.getLatitude(),lon1=this.getLongitude(),lon2=P.getLongitude();
		double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
	}

	public point(Parcel in){

		String[] data  =new String[4];

		in.readStringArray(data);

		this.name = data[0];
		this.latitude =  Double.parseDouble(data[1]) ;
		this.longitude= Double.parseDouble(data[2]) ;
		this.color = Integer.parseInt(data[3]);

	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeStringArray(new String[] {this.name, String.valueOf(this.latitude), String.valueOf(this.longitude),String.valueOf(this.color)});


	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){

		public point createFromParcel(Parcel in){

			return new point(in);
		}

		public point[] newArray(int size){

			return new point[size];
		}


	};
}
